﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniveristyWevApp.BLL;
using UniveristyWevApp.Models;

namespace UniveristyWevApp.Controllers
{
    public class UnAssignCoursesController : Controller
    {
        CourseManager courseManager = new CourseManager();
        [HttpGet]
        public ActionResult UnAssign()
        {
            ViewBag.alart = new AlartType("Unassign all the courses.", "danger");
            return View();
        }
        [HttpPost]
        public ActionResult UnAssign(int? id)
        {
            ViewBag.alart = courseManager.UnAssignCourses();
            return View();
        }
	}
}