﻿using System.Collections.Generic;
using UniveristyWevApp.Gateway;
using UniveristyWevApp.Models;

namespace UniveristyWevApp.BLL
{
    public class RoomManager
    {
        RoomGateway roomGateway=new RoomGateway();
        public IEnumerable<Room> GetAllRooms
        {
            get { return roomGateway.GetAllRooms; }
        }
    }
}