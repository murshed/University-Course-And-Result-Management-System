﻿using System.Collections.Generic;
using UniveristyWevApp.Gateway;
using UniveristyWevApp.Models;

namespace UniveristyWevApp.BLL
{
    public class DayManager
    {
        DayGateway dayGateway=new DayGateway();
        public IEnumerable<Day> GetAllDays
        {
            get { return dayGateway.GetAllDays; }
        } 
    }
}