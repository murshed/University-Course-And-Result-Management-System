/// <autosync enabled="true" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="respond.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="webgrid.js" />
/// <reference path="jquery-3.1.1.slim.js" />
/// <reference path="jquery-3.1.1.js" />
/// <reference path="bootstrap.js" />
/// <reference path="../content/scripts/bootstrap-hover-dropdown.js" />
/// <reference path="jquery-ui.js" />
/// <reference path="bootstrap-datepicker.js" />
/// <reference path="gridmvc.js" />
/// <reference path="mobiscroll-2.9.5.full.js" />
